/**
 * Created by ifedor on 17.11.15.
 */

/* Application class */
var App = function(){

    this.pages = ['users', 'persons', 'positions', 'departments', 'companies'];

    var self = this;

    $('.navlink').bind('click', function(event){

        var pageName = $(this).attr('href').substring(1);
        self.navigate(pageName);

    });

    // hides all pages and sets menu items to unselected
    this.hideAll = function(){
        for(var i = 0; i < this.pages.length; i++){
            $('#' + this.pages[i]).hide();
        }
        $('#navigation').children().removeClass('active');
    };

    //navigates to a selected page
    this.navigate = function(page){
        this.hideAll();
        this.loadData(page);
        $('#' + page).show();
        $('#navigation > li > a[href="#'+ page + '"]').parent().addClass('active');

    };

    // renders datagrid
    this.renderDataGrid = function(page, data){


        var content = "<table class='table table-striped'><thead><tr>";

        for (prop in data[0]){
           content += "<th>"+ prop +"</th>";
        }

        content += "<th>actions</th>";
        content += "</tr></thead>";
        content += "<tbody>";

        for (var i =0; i < data.length; i++){
            content += "<tr>"

            for (var prop in data[i]){
                content += "<td>"+ data[i][prop] +"</td>";
            }

            content += '<td><button type="button" class="btn btn-primary" data-toggle="modal" onclick="openEditWindow(\''+ page + '\', '+ data[i]['id'] + ');">Edit</button><button type="button" class="btn btn-danger">Delete</button></td>';
            content += "</tr>"

        }

        content += "</tbody></table>";
        $('#' + page + ' > div.table_content').html(content);
    };

    // loads JSON data from source
    this.loadData = function(page){
        $.ajax({
            type: 'GET',
            url: 'data/'+ page + '.txt',
            dataType: 'json',
            success: function (data) {

               self.renderDataGrid(page, data);

            }.bind(this),
            error: function (jqXHR, textStatus) {
                console.info(textStatus);
            }.bind(this)
        });
    };
};

var SPApp = new App(); // create app instance
SPApp.navigate('users'); // navigate to "users" page

/* Modal window class*/
var Modal = function(instance, id){

    // gets selected item from data source
    this.getItem = function(data){

        for(var i = 0; i < data.length; i++){
            if(data[i]['id'] == id){
                return data[i];
            }
        }

    };

    // renders modal window content
    this.renderModalContent = function(data){

        var item = this.getItem(data);

        //console.log(item);
        $('#myModalLabel').html(instance + " " + id);

        var content = '';

        for(var prop in item){
            if(prop != "id"){
                content += "<div class='form-group'><label>" + prop +"</label><input type='text' class='form-control' value='" + item[prop]+ "'/></div>";
            }

        }

        $('.modal-body').html(content);

    };

    // loads data from data source
    this.loadData = function(){

        $.ajax({
            type: 'GET',
            url: 'data/'+ instance + '.txt',
            dataType: 'json',
            success: function (data) {

                this.renderModalContent(data);

            }.bind(this),
            error: function (jqXHR, textStatus) {
                console.info(textStatus);
            }.bind(this)
        });

    };

    this.loadData();

    $('#myModal').modal('show');

};


// opens modal window
function openEditWindow(instance, id){
    var modal = new Modal(instance, id);
}





