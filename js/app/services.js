/**
 * Created by ifedor on 20.11.15.
 */

app.service('DataLoader', function($http){

     this.load = function(source){

         return $http({
             method: 'GET',
             url: source
         }).then(function successCallback(response) {
             return response.data;

         }, function errorCallback(response) {
             console.log(response);
         });

     };

});



