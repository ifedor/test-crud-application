/**
 * Created by ifedor on 20.11.15.
 */
app.controller('MenuController', function($scope, $location){
    $scope.getClass = function (path) {

        if ($location.path().substr(0, path.length) === path) {
            return 'active';
        } else {
            return '';
        }
    };

});


app.controller('UsersController', function($scope, $rootScope, DataLoader){


    $scope.openModalWindow = function(item){

        $rootScope.$broadcast('WindowItem', {name:'User', item:item});

    };

    DataLoader.load('data/users.txt').then(function(data) {
        $scope.items = data;
    });

});

app.controller('PersonsController', function($scope,  $rootScope, DataLoader){

    $scope.openModalWindow = function(item){

        $rootScope.$broadcast('WindowItem', {name:'Person', item:item});

    };

    DataLoader.load('data/persons.txt').then(function(data) {
        $scope.items = data;
    });

});

app.controller('PositionsController', function($scope,  $rootScope, DataLoader){

    $scope.openModalWindow = function(item){

        $rootScope.$broadcast('WindowItem', {name:'Position', item:item});

    };


    DataLoader.load('data/positions.txt').then(function(data) {
        $scope.items = data;
    });

});


app.controller('DepartmentsController', function($scope,  $rootScope, DataLoader){


    $scope.openModalWindow = function(item){

        $rootScope.$broadcast('WindowItem', {name:'Department', item:item});

    };


    DataLoader.load('data/departments.txt').then(function(data) {
        $scope.items = data;
    });

});

app.controller('CompaniesController', function($scope,  $rootScope, DataLoader){

    $scope.openModalWindow = function(item){

        $rootScope.$broadcast('WindowItem', {name:'Company', item:item});

    };


    DataLoader.load('data/companies.txt').then(function(data) {
        $scope.items = data;
    });

});

app.controller('ModalWindow', function($scope){

    $scope.$on('WindowItem', function(event, obj) {

        $scope.item = obj.item;
        $('#myModalLabel').html(obj.name + " #" + obj.item.id);
        $('#myModal').modal('show');
        //console.log(item)
    })

});