/**
 * Created by ifedor on 20.11.15.
 */

var app = angular.module('Dashboard', ['ngRoute'])
    .config(function($routeProvider){

        $routeProvider.when('/',
            {
                redirectTo:'/users'
            });

        $routeProvider.when('/users',
            {
                templateUrl:'js/app/views/users.html',
                controller:'UsersController'
            });
        $routeProvider.when('/persons',
            {
                templateUrl:'js/app/views/persons.html',
                controller:'PersonsController'
            });

        $routeProvider.when('/positions',
            {
                templateUrl:'js/app/views/positions.html',
                controller:'PositionsController'
            });

        $routeProvider.when('/departments',
            {
                templateUrl:'js/app/views/departments.html',
                controller:'DepartmentsController'
            });

        $routeProvider.when('/companies',
            {
                templateUrl:'js/app/views/companies.html',
                controller:'CompaniesController'
            });
    });